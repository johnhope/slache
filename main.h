struct Command {
  char *name;
  char *key;
  char *body;
};

struct Command getCommand();
int handleGet(const struct Command command);
int handleSet(const struct Command command);
