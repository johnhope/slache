// Slache is a for-fun-and-learning project to implement a simple in-memory,
// single instance cache.

#include <stdio.h>
#include <string.h>
#include "main.h"

int main() {
  printf("Starting Slache V0.0.1\n\n");

  // Set a one-off start command with no args
  struct Command command = { "START", NULL, NULL };

  while(strcmp(command.name, "EXIT") != 0) {
    printf("> ");
    command = getCommand();

    if (strcmp(command.name, "GET") == 0) {
      handleGet(command);
    } else if (strcmp(command.name, "SET") == 0) {
      handleSet(command);
    } else if (strcmp(command.name, "PING") == 0) {
      printf("PONG\n");
    } else if (strcmp(command.name, "EXIT") == 0 ) {
      printf("Exiting\n");
    } else {
      printf("Unrecognized command %s. Try again.\n", command.name);
    }
  }

  return 0;
}

struct Command getCommand() {
  // Set a size of 255 for now. Includes command name and key but sure.
  char commandLine[255];
  fgets(commandLine, 255, stdin);

  // Remove any trailing (I think?) CR/LF bytes
  commandLine[strcspn(commandLine, "\r\n")] = 0;

  // Copy the string to prevent the original being altered by strtok
  char commandLineCopy[255];
  strcpy(commandLineCopy, commandLine);

  // Get the command name from the first word of the line
  char *commandName = strtok(commandLineCopy, " ");
  char *commandKey = strtok(NULL, " ");
  char *commandBody = strtok(NULL, " ");

  // Create a Command struct with these attributes
  struct Command command = { commandName, commandKey, commandBody };

  return command;
}

int handleGet(const struct Command command) {
  printf("%s\n", command.key); // print out the command key

  return 0;
}

int handleSet(const struct Command command) {
  printf("%s\n", command.key); // print out the command key
  printf("%s\n", command.body); // print out the command body

  return 0;
}
